#include <stdio.h>
#include <stdlib.h>

#include "gen.h"
#include "asm.h"
#include "commongen.h"

#define ASP "r15"
#define BSP "r14"
#define CSP "rsp"
#define DP  "r13"
#define HP  "r12"
#define FC  "rbx"

struct node {
	void *red_code;
	void *desc;
	long p;
} __attribute__((packed));

static void loadAstack(char *reg, int off, FILE *ofile);
static void aotmemcopy(char *regs, char *regd, char *bufreg, int n,
                       FILE *ofile);

static void create(struct arg args[4], FILE *ofile);
static void fill(struct arg args[4], FILE *ofile);
static void fillI(struct arg args[4], FILE *ofile);
static void fillB(struct arg args[4], FILE *ofile);
static void fill_A(struct arg args[4], FILE *ofile);
static void set_entry(struct arg args[4], FILE *ofile);
static void add_args(struct arg args[4], FILE *ofile);
static void del_args(struct arg args[4], FILE *ofile);
static void push_args(struct arg args[4], FILE *ofile);
static void pushI_A(struct arg args[4], FILE *ofile);
static void get_node_arity(struct arg args[4], FILE *ofile);
static void get_desc_arity(struct arg args[4], FILE *ofile);
static void push_ap_arity(struct arg args[4], FILE *ofile);
static void eq_desc(struct arg args[4], FILE *ofile);
static void eqi_A(struct arg args[4], FILE *ofile);
static void eqi_B(struct arg args[4], FILE *ofile);
static void pop_A(struct arg args[4], FILE *ofile);
static void push_A(struct arg args[4], FILE *ofile);
static void update_A(struct arg args[4], FILE *ofile);
static void pushI(struct arg args[4], FILE *ofile);
static void jmp(struct arg args[4], FILE *ofile);
static void jsr(struct arg args[4], FILE *ofile);
static void jmp_false(struct arg args[4], FILE *ofile);
static void jmp_true(struct arg args[4], FILE *ofile);
static void rtn(struct arg args[4], FILE *ofile);
static void jsr_eval(struct arg args[4], FILE *ofile);
static void halt(struct arg args[4], FILE *ofile);
static void dump(struct arg args[4], FILE *ofile);
static void print(struct arg args[4], FILE *ofile);
static void print_symbol(struct arg args[4], FILE *ofile);
static void label(struct arg args[4], FILE *ofile);

char preamble[] =
	"extern _rtx86_64_node_arity\n"
	"extern _rtx86_64_memcopy\n"
	"extern _rtx86_64_halt\n"
	"global _start\n"
	"section .text\n"
	"_start:\n"
	"\tpush\trbx\n"
	"\tpush\tr12\n"
	"\tpush\tr13\n"
	"\tpush\tr14\n"
	"\tpush\tr15\n"
	"\tpush\trbp\n"
	"\tmov\t"ASP", _astack\n"
	"\tmov\t"BSP", _bstack\n"
	"\tmov\t"HP", _heap\n"
	"\tmov\t"FC", 4096\n"
	;

char postamble[] =
	"_end:\n"
	"\tpop\trbp\n"
	"\tpop\tr15\n"
	"\tpop\tr14\n"
	"\tpop\tr13\n"
	"\tpop\tr12\n"
	"\tpop\trbx\n"
	"section .bss\n"
	"_astack:\tresb 4096\n"
	"_bstack:\tresb 4096\n"
	"_heap:\t\tresq 4096\n";

void (*instrs[])(struct arg *, FILE *) = {
	[CREATE] = create,
	[FILL] = fill,
	[FILLI] = fillI,
	[FILLB] = fillB,
	[FILL_A] = fill_A,
	[SET_ENTRY] = set_entry,
	[ADD_ARGS] = add_args,
	[DEL_ARGS] = del_args,
	[PUSH_ARGS] = push_args,
	[PUSHI_A] = pushI_A,
	[GET_NODE_ARITY] = get_node_arity,
	[GET_DESC_ARITY] = get_desc_arity,
	[PUSH_AP_ARITY] = push_ap_arity,
	[EQ_DESC] = eq_desc,
	[EQI_A] = eqi_A,
	[EQI_B] = eqi_B,
	[POP_A] = pop_A,
	[PUSH_A] = push_A,
	[UPDATE_A] = update_A,
	[PUSHI] = pushI,
	[JMP] = jmp,
	[JSR] = jsr,
	[JMP_FALSE] = jmp_false,
	[JMP_TRUE] = jmp_true,
	[RTN] = rtn,
	[JSR_EVAL] = jsr_eval,
	[HALT] = halt,
	[DUMP] = dump,
	[PRINT] = print,
	[PRINT_SYMBOL] = print_symbol,
	[LABEL] = label
};

struct cg x86_64 = {
	.arch = "x86_64",
	.preamble = preamble,
	.postamble = postamble,
	.instrs = instrs
};

static void
loadAstack(char *reg, int off, FILE *ofile)
{
	fprintf(ofile, "\tmov\t%s, [%s - %d]", reg, ASP, off * 8);
}

static void
aotmemcopy(char *regs, char *regd, char *bufreg, int n, FILE *ofile)
{
	for (int i = 0; i < n; ++i) {
		fprintf(ofile, "\tmov\t%s, [%s + %d]\n", bufreg, regs, 8 * i);
		fprintf(ofile, "\tmov\t[%s + %d], %s\n", regd, 8 * i, bufreg);
	}
}

static void
create(struct arg args[4], FILE *ofile)
{
	/* Push the address on the heap on the A-Stack. */
	fprintf(ofile, "\tmov\t[%s], %s", ASP, HP);
	fprintf(ofile, "\tadd\t%s, 8", ASP);

	/* Create the new node. */
	fprintf(ofile, "\tadd\t%s, %d\n", HP, 3);
	fprintf(ofile, "\tsub\t%s, %d\n", FC, 3);
}

static void
fill(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[3].n, ofile);

	/* Copy the address of the reduction code */
	fprintf(ofile, "\tmov\tQWORD [rax], %s\n", args[2].label);

	/* Fills the descriptor. */
	fprintf(ofile, "\tlea\trcx, [%s + %d]\n", args[0].label, 4 * args[1].n);
	fprintf(ofile, "\tmov\t[rax + 8], rcx\n");

	/* Adds the arguments. */
	fprintf(ofile, "\tmov\t[rax + 16], %s\n", HP);
	aotmemcopy(ASP, HP, "rcx", args[1].n, ofile);

	fprintf(ofile, "\tsub\t%s, %d\n", ASP, args[1].n);
	fprintf(ofile, "\tadd%s, %d\n", HP, args[1].n * 3);
	fprintf(ofile, "\tsub%s, %d\n", FC, args[1].n * 3);
}

static void
fillI(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[1].n, ofile);

	/* Fills the node information with defaults. */
	fprintf(ofile, "\tmov\tQWORD [rax], _hnf\n");
	fprintf(ofile, "\tmov\tQWORD [rax + 8], _INT\n");

	/* Add the integer. */
	fprintf(ofile, "\tmov\tQWORD [rax + 16], %d\n", args[0].n);
}

static void
fillB(struct arg args[4], FILE *ofile)
{
	/* Get the node address. */
	loadAstack("rax", args[1].n, ofile);

	/* Fills the node information with defaults. */
	fprintf(ofile, "\tmov\tQWORD [rax], _hnf\n");
	fprintf(ofile, "\tmov\tQWORD [rax + 8], _BOOL\n");

	/* Add the integer. */
	fprintf(ofile, "\tmov\tBYTE [rax + 16], %d\n", args[0].n);
}

static void
fill_A(struct arg args[4], FILE *ofile)
{
	/* Get nodes addresses. */
	loadAstack("rdi", args[0].n, ofile);
	loadAstack("rsi", args[1].n, ofile);

	/* Copy memory. */
	fprintf(ofile, "\tmov\trdx, 3\n");
	fprintf(ofile, "\tcall\t_rtx86_64_memcopy\n");
}

static void
set_entry(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tmov\trax, [%s - %d]\n", ASP, args[1].n);
	fprintf(ofile, "\tmov\tQWORD [rax], %s\n", args[0].label);
}

static void
add_args(struct arg args[4], FILE *ofile)
{
	loadAstack("rdi", args[0].n, ofile);
	loadAstack("rcx", args[2].n, ofile);

	aotmemcopy("rdi", "rcx", "rdx", 2, ofile);

	/* Adapt the descriptor to hold more arguments. */
	fprintf(ofile, "\tadd\tQWORD [rcx + 8], %d\n", args[1].n * 4);

	/* Adds the arguments. */
	fprintf(ofile, "\tmov\t[rcx + 16], %s\n", HP);

	/* Get the arguments from the first node. */
	fprintf(ofile, "\tcall\t_rtx86_64_node_arity\n");
	fprintf(ofile, "\tmov\trsi, %s\n", HP);
	fprintf(ofile, "\tmov\trdx, rax\n");
	fprintf(ofile, "\tmul\t3\n");
	fprintf(ofile, "\tsub\t%s, rax\n", FC);
	fprintf(ofile, "\tadd\t%s, rax\n", HP);
	fprintf(ofile, "\tmov\trdi, [rdi + 16]\n");
	fprintf(ofile, "\tcall\t_rtx86_64_memcopy\n");

	/* Get arguments from the stack. */
	aotmemcopy(ASP, HP, "rsi", args[1].n, ofile);

	fprintf(ofile, "\tadd\t%s, %d\n", ASP, args[1].n);
	fprintf(ofile, "\tsub%s, %d\n", FC, 3 * args[1].n);
	fprintf(ofile, "\tadd%s, %d\n", HP, 3 * args[1].n);
}

static void
del_args(struct arg args[4], FILE *ofile)
{
	loadAstack("rcx", args[0].n, ofile);
	loadAstack("rdi", args[2].n, ofile);

	/* Copy information to the second node. */
	aotmemcopy("rcx", "rdi", "rcx", 3, ofile);

	/* Adapt the descriptor to hold less arguments. */
	fprintf(ofile, "\tsub\tQWORD [rdi + 8], %d\n", args[1].n * 4);

	/* Adds the remaining arguments on the A-stack. */
	fprintf(ofile, "\tcall\t_rtx86_64_node_arity\n");
	fprintf(ofile, "\tsub\trax, %d\n", args[1].n);
	fprintf(ofile, "\tshr\trax, 3\n");
	fprintf(ofile, "\tmul\t3\n");
	fprintf(ofile, "\tadd\trax, [rdi + 16]\n");
	fprintf(ofile, "\tlea\trsi, [%s + %d]\n", ASP, args[1].n * 8);
	aotmemcopy("rsi", "rax", "", args[1].n, ofile);
	fprintf(ofile, "\tmov\t%s, rsi", ASP);
	fprintf(ofile, "\tadd\trsi, 8\n");
}

static void
push_args(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[0].n, ofile);
	fprintf(ofile, "\tmov\trax, [rax + 16]\n");
	fprintf(ofile, "\tadd\t%s, %d\n", ASP, args[2].n);
	aotmemcopy("rax", ASP, "rdx", args[2].n, ofile);
}

static void
pushI_A(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[0].n, ofile);
	fprintf(ofile, "\tmov\trdx, QWORD [rax + 16]\n");
	fprintf(ofile, "\tmov\tQWORD [%s], rdx\n", BSP);
	fprintf(ofile, "\tadd\t%s, 8\n", BSP);
}

static void
get_node_arity(struct arg args[4], FILE *ofile)
{
	loadAstack("rdi", args[0].n, ofile);
	fprintf(ofile, "\tcall\t_rtx86_64_node_arity\n");

	/* Push the value on the B-Stack. */
	fprintf(ofile, "\tmov\t[%s], rax\n", BSP);
	fprintf(ofile, "\tadd\t%s, 8", BSP);
}

static void
get_desc_arity(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[0].n, ofile);

	/* Get the descriptor address. */
	fprintf(ofile, "\tmov\trax, [rax + 8]\n");

	/* Get the descriptor value. */
	fprintf(ofile, "\tmov\tecx, [rax]\n");
	fprintf(ofile, "\tsub\trax, rcx\n");
	fprintf(ofile, "\tsub\trax, 8\n");
	fprintf(ofile, "\tmov\trax, [rax]\n");

	/* Push the value on the B-Stack. */
	fprintf(ofile, "\tmov\t[%s], rax\n", BSP);
	fprintf(ofile, "\tadd\t%s, 8", BSP);
}

static void
push_ap_arity(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[0].n, ofile);

	/* Get the descriptor address. */
	fprintf(ofile, "\tmov\trax, [rax + 8]\n");

	/* Get the address of the reduction code. */
	fprintf(ofile, "\tmov\tecx, [rax]\n");
	fprintf(ofile, "\tsub\trax, rcx\n");
	fprintf(ofile, "\tsub\trax, 8\n");
	fprintf(ofile, "\tmov\trax, [rax]\n");

	/* Push the address of the reduction code. */
	fprintf(ofile, "\tpush\trax\n");
}

static void
eq_desc(struct arg args[4], FILE *ofile)
{
	loadAstack("rcx", args[2].n, ofile);
	fprintf(ofile, "\tlea\trax, [%s + %d]\n", args[0].label, args[1].n * 4);

	fprintf(ofile, "\tcmp\trax, [rcx + 8]\n");
	fprintf(ofile, "\txor\tch, ch\n");
	fprintf(ofile, "\tcmove\tch, 1\n");
	fprintf(ofile, "\tmov\t[%s], ch\n", BSP);
	fprintf(ofile, "\tadd\t%s, 1\n", BSP);
}

static void
eqi_A(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[1].n, ofile);
	fprintf(ofile, "\tmov\trax, [rax + 16]\n");
	fprintf(ofile, "\txor\tch, ch\n");
	fprintf(ofile, "\tcmp\trax, %d\n", args[0].n);
	fprintf(ofile, "\tcmove\tch, 1\n");
	fprintf(ofile, "\tmov\t[%s], ch\n", BSP);
	fprintf(ofile, "\tadd\t%s, 1\n", BSP);
}

static void
eqi_B(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[1].n, ofile);
	fprintf(ofile, "\tmov\tBYTE [%s], 0\n", BSP);
	fprintf(ofile, "\tcmp\trax, %d\n", args[0].n);
	fprintf(ofile, "\tcmove\tBYTE [%s], 1", BSP);
	fprintf(ofile, "\tadd\t%s, 1\n", BSP);
}

static void
pop_A(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tsub\t%s, %d\n", ASP, 8 * args[0].n);
}

static void
push_A(struct arg args[4], FILE *ofile)
{
	loadAstack("rax", args[0].n, ofile);
	fprintf(ofile, "\tmov\t[%s], rax\n", ASP);
	fprintf(ofile, "\tadd\t%s, 8\n", ASP);
}

static void
update_A(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tmov\trax, QWORD [%s + %d]\n", ASP, args[0].n);
	fprintf(ofile, "\tmov\tQWORD [%s + %d], rax\n", ASP, args[1].n);
}

static void
pushI(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tmov\tQWORD [%s], %d\n", BSP, args[0].n);
	fprintf(ofile, "\tadd\t%s, 8\n", BSP);
}

static void
jmp(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tjmp\t%s\n", args[0].label);
}

static void
jsr(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tcall\t%s\n", args[0].label);
}

static void
jmp_false(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tmov\tah, [%s]\n", BSP);
	fprintf(ofile, "\tcmp\tah, 0\n");
	fprintf(ofile, "\tje\t%s\n", args[0].label);
}

static void
jmp_true(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tmov\tah, [%s]\n", BSP);
	fprintf(ofile, "\tcmp\tah, 0\n");
	fprintf(ofile, "\tjne\t%s\n", args[0].label);
}

static void
rtn(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tret\n");
}

static void
jsr_eval(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tmov\trax, [%s]\n", ASP);
	fprintf(ofile, "\tcall\t[rax]\n");
}

static void
halt(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "\tcall\t_halt\n");
}

static void
dump(struct arg args[4], FILE *ofile)
{
}

static void
print(struct arg args[4], FILE *ofile)
{
}

static void
print_symbol(struct arg args[4], FILE *ofile)
{
}

static void
label(struct arg args[4], FILE *ofile)
{
	fprintf(ofile, "%s:\n", args[0].label);
}
