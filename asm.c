#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "asm.h"
#include "utils.h"

#define ASMCHUNK 128

char *instruction_table[] = {
	[CREATE] = "create",
	[FILL] = "fill",
	[FILLI] = "fillI",
	[FILLB] = "fillB",
	[FILL_A] = "fill_A",
	[SET_ENTRY] = "set_entry",
	[ADD_ARGS] = "add_args",
	[DEL_ARGS] = "del_args",
	[PUSH_ARGS] = "push_args",
	[PUSHI_A] = "pushI_A",
	[GET_NODE_ARITY] = "get_node_arity",
	[GET_DESC_ARITY] = "get_desc_arity",
	[PUSH_AP_ARITY] = "push_ap_arity",
	[EQ_DESC] = "eq_desc",
	[EQI_A] = "eqI_A",
	[EQI_B] = "eqI_B",
	[EQB_A] = "eqB_A",
	[EQB_B] = "eqB_B",
	[POP_A] = "pop_A",
	[PUSH_A] = "push_A",
	[UPDATE_A] = "update_A",
	[POP_B] = "pop_B",
	[PUSH_B] = "push_B",
	[UPDATE_B] = "update_B",
	[PUSHI] = "pushI",
	[PUSHB] = "pushB",
	[JMP] = "jmp",
	[JSR] = "jsr",
	[JMP_FALSE] = "jmp_false",
	[JMP_TRUE] = "jmp_true",
	[RTN] = "rtn",
	[JSR_EVAL] = "jsr_eval",
	[JMP_EVAL] = "jmp_eval",
	[HALT] = "halt",
	[DUMP] = "dump",
	[PRINT] = "print",
	[PRINT_SYMBOL] = "print_symbol"
};

int arity[] = {
	[CREATE] = 0,
	[FILL] = 4,
	[FILLB] = 2,
	[FILLI] = 2,
	[FILL_A] = 2,
	[SET_ENTRY] = 2,
	[ADD_ARGS] = 3,
	[DEL_ARGS] = 3,
	[PUSH_ARGS] = 3,
	[PUSHI_A] = 1,
	[GET_NODE_ARITY] = 1,
	[GET_DESC_ARITY] = 1,
	[PUSH_AP_ARITY] = 1,
	[EQ_DESC] = 3,
	[EQI_A] = 2,
	[EQI_B] = 2,
	[EQB_A] = 2,
	[EQB_B] = 2,
	[POP_A] = 1,
	[PUSH_A] = 1,
	[UPDATE_A] = 2,
	[POP_B] = 1,
	[PUSH_B] = 1,
	[UPDATE_B] = 2,
	[PUSHI] = 1,
	[JMP] = 1,
	[JSR] = 1,
	[JMP_FALSE] = 1,
	[JMP_TRUE] = 1,
	[RTN] = 0,
	[JSR_EVAL] = 0,
	[JMP_EVAL] = 0,
	[HALT] = 0,
	[DUMP] = 1,
	[PRINT] = 1,
	[PRINT_SYMBOL] = 1
};

static char ident[MAX_IDENT_SIZE];
static FILE *infile;

static int isid(int c);
static void getident(void);
static void skip_spaces(void);
static void getint(struct arg *p);
static struct arg getarg(void);
static struct expr getexpr(void);

static int
isid(int c)
{
	return isalnum(c) || c == '.' || c == '_';
}

static void
getident(void)
{
	int i;

	i = 0;
	while (isid(ident[i++] = getc(infile)));
	ungetc(ident[i - 1], infile);
	ident[i - 1] = '\0';
}

static void
skip_spaces(void)
{
	int c;

	while ((c = getc(infile)) == ' ' || c == '\t');
	ungetc(c, infile);
}

static void
getint(struct arg *p)
{
	int c;

	p->n = 0;
	while (isdigit(c = getc(infile)))
		p->n = p->n * 10 + c - '0';
	ungetc(c, infile);
}

static struct arg
getarg(void)
{
	int c;
	struct arg arg;

	c = getc(infile);
	if (isdigit(c)) {
		ungetc(c, infile);
		getint(&arg);
		arg.type = A_ADDR;
	} else if (c == '+') {
		arg.type = A_INT;
		getint(&arg);
	} else if (isid(c)) {
		char *p = arg.label;
		while (isid(*(p++) = getc(infile)));
		ungetc(*(p - 1), infile);
		*(p - 1) = '\0';
		arg.type = A_LABEL;
		ungetc(c, infile);
	} else {
		exit(1);
	}
	return arg;
}

static struct expr
getexpr(void)
{
	struct expr e;
	enum instruction i;
	int c;

	skip_spaces();
	getident();
	for (i = 0; i <= PRINT_SYMBOL; ++i) {
		if (!strcmp(ident, instruction_table[i]))
			break;
	}

	skip_spaces();
	e.instr = i;
	if (i == LABEL) {
		if (getc(infile) == ':') {
			strcpy(e.args[0].label, ident);
			skip_spaces();
		} else {
			exit(1);
		}
	} else {
		for (int j = 0; j < arity[i]; ++j) {
			e.args[j] = getarg();
			skip_spaces();
		}
	}
	if ((c = getc(infile)) == EOF) {
		ungetc(c, infile);
	} else if (c != '\n') {
		exit(1);
	}
	return e;
}

struct expr *
asmmain(int *len, char *file)
{
	struct expr *p;
	int maxlen;
	int c;

	infile = fopen(file, "r");
	if (!infile)
		exit(1);

	*len = 0;
	maxlen = ASMCHUNK;
	p = emalloc(maxlen * sizeof(struct expr));
	while ((c = fgetc(infile)) != EOF) {
		ungetc(c, infile);
		p[(*len)++] = getexpr();
		if (maxlen <= *len) {
			maxlen += ASMCHUNK;
			p = realloc(p, maxlen * sizeof(struct expr));
		}
	}
	return p;
}
