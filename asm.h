#define MAX_IDENT_SIZE 256

enum instruction {
	CREATE, FILL, FILLI, FILLB, FILL_A, SET_ENTRY, ADD_ARGS, DEL_ARGS,
	PUSH_ARGS, PUSHI_A, GET_NODE_ARITY, GET_DESC_ARITY, PUSH_AP_ARITY,
	EQ_DESC, EQI_A, EQI_B, EQB_A, EQB_B, POP_A, PUSH_A, UPDATE_A, POP_B,
	PUSH_B, UPDATE_B, PUSHI, PUSHB, JMP, JSR, JMP_FALSE, JMP_TRUE, RTN,
	JSR_EVAL, JMP_EVAL, HALT, DUMP, PRINT, PRINT_SYMBOL, LABEL
};

struct expr {
	enum instruction instr;
	struct arg {
		union {
			char label[MAX_IDENT_SIZE];
			int n;
		};
		enum {
			A_LABEL, A_INT, A_ADDR
		} type;
	} args[4];
};

struct expr *asmmain(int *len, char *file);
extern int arity[];
extern char *instruction_table[];
