CC=cc
CFLAGS=-Wall -I.
LD=cc

O=o

HFILES= asm.h\
	x86_64/gen.h\
	utils.h\
	dag.h\
	disasm.h\
	commongen.h

BIN=gc

OFILES= gc.$O\
	asm.$O\
	x86_64/gen.$O\
	utils.$O\
	dag.$O\
	disasm.$O\
	commongen.$O

all:V: $BIN

%.$O: %.c
	$CC $CFLAGS -c $stem.c -o $stem.$O

$BIN: $OFILES $HFILES
	$CC $CFLAGS $OFILES -o $BIN