#include <stdio.h>
#include <stdlib.h>

#include "asm.h"
#include "commongen.h"

void
gencode(int len, struct expr *prog, struct cg cg, FILE *ofile)
{
	fprintf(ofile, "%s", cg.preamble);
	for (int i = 0; i < len; ++i)
		cg.instrs[prog[i].instr](prog[i].args, ofile);
	fprintf(ofile, "%s", cg.postamble);
}
