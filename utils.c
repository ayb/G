#include <stdlib.h>
#include <stdio.h>

#include "utils.h"

void *
emalloc(size_t size)
{
	void *p;

	p = malloc(size);
	if (p == NULL)
		exit(1);
	return p;
}

void *
erealloc(void *p, size_t size)
{
	p = realloc(p, size);
	if (p == NULL)
		exit(1);
	return p;
}
