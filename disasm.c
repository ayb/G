#include <stdio.h>
#include <stdlib.h>

#include "asm.h"
#include "disasm.h"

void
disasm(struct expr *p, int len, char *file)
{
	FILE *outfile;

	outfile = fopen(file, "w");
	if (!outfile)
		exit(1);

	for (int i = 0; i < len; ++i) {
		if (p[i].instr == LABEL) {
			fprintf(outfile, "%s:\n", p[i].args[0].label);
			continue;
		}
		fprintf(outfile, "\t%s", instruction_table[p[i].instr]);
		for (int j = 0; j < arity[p[i].instr]; ++j) {
			switch (p[i].args[j].type) {
			case A_INT:
				fprintf(outfile, "+");
			case A_ADDR:
				fprintf(outfile, "%d ", p[i].args[j].n);
				break;
			case A_LABEL:
				fprintf(outfile, "%s ",
				        p[i].args[j].label);
				break;
			}
		}
		break;
	}
}
