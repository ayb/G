#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "asm.h"
#include "disasm.h"
#include "x86_64/gen.h"
#include "commongen.h"

int
main(int argc, char **argv)
{
	int len;
	struct expr *l;
	FILE *ofile;

	l = asmmain(&len, "test.abc");
	ofile = fopen("out.S", "w");

	gencode(len, l, x86_64, ofile);
	fclose(ofile);
}
