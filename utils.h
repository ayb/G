#include <stddef.h>

void *emalloc(size_t size);
void *erealloc(void *p, size_t size);
