struct _IO_FILE;
typedef struct _IO_FILE FILE;

struct arg;
struct expr;

struct cg {
	char *preamble;
	char *postamble;
	char *arch;
	void (*add_desc_entry)(char *, int, FILE *);
	void (**instrs)(struct arg *, FILE *);
};

void gencode(int len, struct expr *prog, struct cg cg, FILE *ofile);
